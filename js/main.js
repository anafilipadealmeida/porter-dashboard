/**
 * This function guarantees the page is ready and calls all the custom code.
 **/
$(function() {
	expandList();
	showOpenMenu();
	hideOpenMenu();
	showLogoutMenu();
	hideLogoutMenu();
	showDeleteMenu();
	showNewAdminWindow();
	hideNewAdminWindow();
	readURL();
	dragRows();
	handleInlineInputs();
	// initDateTimePicker();
});

/**
 * This function binds the click event for the expanded list of places.
 **/
function expandList() {
	// Check if the list is already expanded or not.
	$(".table__column-icon.icon.icon-expand").click(function(){
		var $tableRow = $(this).closest(".table__row");

		// If row is already expanded:
		if ($tableRow.hasClass('table__row--is-expanded')) {
			// Collapse this row.
			$tableRow.removeClass('table__row--is-expanded');
		}
		// If is not expanded:
		else {
			// Expand this row.
			$tableRow.addClass('table__row--is-expanded');
		}
	});
}

/**
 * This function binds the click event to show the open menu.
 **/
function showOpenMenu() {
	$(".menu__icon").click(function(){
		$(".menu__open").addClass('menu__open--active');
	});
}

/**
 * This function binds the click event to hide the open menu.
 **/
function hideOpenMenu() {
	$(".icon.icon-menu_active").click(function(){
		$(".menu__open").removeClass('menu__open--active');

		// If menu logout is still opened:
		if($(".menu__logout").hasClass('menu__logout--active')) {
			$(".menu__logout").removeClass('menu__logout--active');
		}
	});
}

/**
 * This function binds the click event to show the logout menu.
 **/
function showLogoutMenu() {
	$(".menu__open-link[href='#logout']").click(function(){
		$(".menu__logout").addClass('menu__logout--active');
	});
}

/**
 * This function binds the click event to hide the logout menu.
 **/
function hideLogoutMenu() {
	$(".menu__logout-button.menu__logout-button--inverse").click(function(){
		$(".menu__logout").removeClass('menu__logout--active');
	});
}

/**
 * This function binds the click event to hide and show the delete menu.
 **/
function showDeleteMenu() {
	// Check if the menu is already open or not.
	$(".page-header__button.page-header__button--delete").click(function(){
		// If menu is already open:
		if ($(this).hasClass('page-header__button--delete-active')) {
			// Hide this menu.
			$(this).removeClass('page-header__button--delete-active');
			$(".menu__delete").removeClass('menu__delete--active');
		}
		// If is not open:
		else {
			// Show this menu.
			$(this).addClass('page-header__button--delete-active');
			$(".menu__delete").addClass('menu__delete--active');
		}
	});
	// Hides delete menu when user clicks on "no" button.
	$(".menu__delete-button.menu__delete-button--inverse").click(function(){
		// If menu is already open:
		if ($(".page-header__button.page-header__button--delete").hasClass('page-header__button--delete-active')) {
			// Hide this menu.
			$(".page-header__button.page-header__button--delete").removeClass('page-header__button--delete-active');
			$(".menu__delete").removeClass('menu__delete--active');
		}
	});
}

/**
 * This function binds the click event to show the add new admin modal window.
 **/
function showNewAdminWindow() {
	$(".page-column__add-result").click(function(){
		$(".lightbox.lightbox--admin").addClass('lightbox--open');
	});
}

/**
 * This function binds the click event to hide the add new admin modal window.
 **/
function hideNewAdminWindow() {
	$(".new-admin__button").click(function(){
		// If user clicks in cancel or skip buttons:
		if ($(this).hasClass('new-admin__button--cancel') || $(this).hasClass('new-admin__button--skip')) {
			// Hide modal window.
			$(".lightbox.lightbox--admin.lightbox--open").removeClass('lightbox--open');
		}
	});
}

/**
 * This function binds the click event to upload an image to the profile.
 **/
function readURL(input) {

    if (input !== undefined && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('.form__column.form__column--picture').css('background-image', 'url(' + e.target.result + ')').text('');
        }

        reader.readAsDataURL(input.files[0]);
    }
}

$(".form__column.form__column--picture-input").change(function(){
    readURL(this);
});

/**
 * This function binds the drag related events for the expanded list of places.
 **/
function dragRows() {
	var $draggedElement;

	// Drop zones:
	$(".table__row.table__row--bigfont").on('dragover', function(e) {
		$(this).addClass('table__row--dropzone').siblings().removeClass('table__row--dropzone');

		if (e.preventDefault) {
			e.preventDefault(); // Necessary. Allows us to drop.
		}
		e.originalEvent.dataTransfer.dropEffect = 'move';
		return false;
	});

	$(".table__row.table__row--bigfont").on('drop', function(event) {
		var $targetTable = $(this).find(".table__expanded"),
		$droppedElement;

		// Check if we're dropping into the same table:
		if ($targetTable.is($draggedElement.parent())) {
			$droppedElement = $draggedElement;
		}
		else {
			// Add the element to end of the list.
			$droppedElement = $draggedElement.appendTo($targetTable);

			// If previous sibling has light background, remove class - else add.
			if ($droppedElement.prev(".table__row.table__row--smallfont[draggable]").hasClass('table__row--light')) {
				$droppedElement.removeClass('table__row--light');
			}
			else {
				$droppedElement.addClass('table__row--light');
			}
		}

		// Apply common effects after dropping:
		$droppedElement.show().removeClass('table__row--hide-before').nextAll().toggleClass('table__row--light');
		$targetTable.closest(".table__row--dropzone").removeClass('table__row--dropzone');

		// Reset helper variables:
		$draggedElement = undefined;
		return false;
	});

	// Draggable elements:
	$(".table__row.table__row--smallfont[draggable]").on('dragstart', function(event){
		event.originalEvent.dataTransfer.effectAllowed = 'move';
		// event.originalEvent.dataTransfer.setData('text/html', $(this)[0].outerHTML);
		$(this)
			.addClass('table__row--hide-before')
			.nextAll().toggleClass('table__row--light');

		$(this)
			.closest(".table__row.table__row--bigfont").addClass('table__row--dropzone');
	});

	$(".table__row.table__row--smallfont[draggable]").on('drag', function(event) {
		// Only set new values if they are undefined:
		$draggedElement = $draggedElement || $(this).hide();
		return false;
	});
}

/**
 * This function binds click events to handle inline forms in expanded tables.
 **/
function handleInlineInputs() {
	$(".table__column-icon.icon.icon-edit").click(function(){
		// Enables input on the same row
		var $input =
		$(this)
			.eq(0)
			.closest(".table__row")
			.children(".form")
			.addClass("form--active")
			.children(".form__input")
			.removeAttr('disabled')
			.focus();

		// Move cursor to the end of the value and bind focusout event.
		$input.val($input.val())
			.focusout(function(){
				$(this)
					.attr('disabled', 'disabled')
					.parent()
					.removeClass('form--active');
		});
	});
}


/**
 * This function initializes the datetimepickers.
 **/
// function initDateTimePicker() {
// 	$("#date").datetimepicker({
// 		defaultDate: moment(new Date),
// 		format: 'D-MMM',
// 		widgetParent: $("#date").parent(),
// 		debug: true
// 	});
// }